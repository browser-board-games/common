use std::collections::HashMap;

use serde::{Deserialize, Serialize};
use thiserror::Error;
use wgt_common::{EventReach, Game, UserID};

pub const PLAYER_COUNT: usize = 2;
pub const BOARD_SIZE: usize = 9;

#[derive(Clone, Deserialize, Serialize)]
pub struct TicTacToe {
    pub players: HashMap<UserID, (String, bool)>,
    pub board: [Option<bool>; BOARD_SIZE],
}

#[derive(Clone, Deserialize, Serialize)]
pub enum TttEvent {
    Error { error: TttError },
    State { state: TicTacToe },
    End { winner: Option<String> },
}

#[derive(Clone, Copy, Deserialize, Serialize)]
pub struct TttMove(pub usize);

#[derive(Clone, Copy, Debug, Error, Deserialize, Serialize)]
pub enum TttError {
    #[error("Game can only be played with two players")]
    InvalidPlayerCount,
    #[error("Invalid player")]
    InvalidPlayer,
    #[error("Not your turn")]
    NotYourTurn,
    #[error("Invalid location")]
    InvalidLocation,
    #[error("Place already taken")]
    AlreadyTaken,
}

type Result<T> = std::result::Result<T, TttError>;

impl TicTacToe {
    fn uid(&self, mark: bool) -> Result<&UserID> {
        self.players
            .iter()
            .find_map(|(uid, (_, pmark))| (&mark == pmark).then_some(uid))
            .ok_or(TttError::InvalidPlayer)
    }

    pub fn turn(&self) -> Result<&UserID> {
        self.uid(
            self.board
                .iter()
                .filter_map(|opt| opt.map(|v| if v { -1 } else { 1 }))
                .sum::<i32>()
                != 0,
        )
    }

    fn is_end(&self) -> bool {
        self.board.iter().all(Option::is_some)
    }

    #[inline]
    fn check_pos(&self, pos: usize, mark: bool) -> bool {
        self.board[pos] == Some(mark)
    }

    fn winner(&self) -> Option<&UserID> {
        const LINES: [[usize; 3]; 8] = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6],
        ];
        for mark in [true, false] {
            for line in LINES {
                if self.check_pos(line[0], mark)
                    && self.check_pos(line[1], mark)
                    && self.check_pos(line[2], mark)
                {
                    return self.uid(mark).ok();
                }
            }
        }

        None
    }
}

impl Game for TicTacToe {
    type Event = TttEvent;
    type Move = TttMove;
    type Error = TttError;

    fn new(players: HashMap<UserID, String>) -> Result<Self> {
        if players.len() != PLAYER_COUNT {
            Err(Self::Error::InvalidPlayerCount)
        } else {
            Ok(Self {
                players: players
                    .into_iter()
                    .zip([false, true].into_iter())
                    .map(|((uid, name), side)| (uid, (name, side)))
                    .collect(),
                board: [None; BOARD_SIZE],
            })
        }
    }

    fn broadcast_state(&self) -> Vec<(Self::Event, EventReach)> {
        let mut events = vec![(
            Self::Event::State {
                state: self.clone(),
            },
            EventReach::Broadcast,
        )];
        if let Some(winner) = self.winner() {
            events.push((
                Self::Event::End {
                    winner: Some(
                        self.players
                            .get(winner)
                            .map(|p| p.0.as_str())
                            .unwrap_or("")
                            .to_string(),
                    ),
                },
                EventReach::Broadcast,
            ));
        } else if self.is_end() {
            events.push((Self::Event::End { winner: None }, EventReach::Broadcast));
        }
        events
    }

    fn state_event_for(&self, _: &UserID) -> Self::Event {
        Self::Event::State {
            state: self.clone(),
        }
    }

    fn handle_move(
        &mut self,
        mov: Self::Move,
        uid: &UserID,
    ) -> Result<Vec<(Self::Event, EventReach)>> {
        match self.players.get(uid) {
            Some((_, mark)) => {
                if self.turn()? != uid {
                    Err(TttError::NotYourTurn)
                } else if mov.0 >= BOARD_SIZE {
                    Err(TttError::InvalidLocation)
                } else if self.board[mov.0].is_some() {
                    Err(TttError::AlreadyTaken)
                } else {
                    self.board[mov.0] = Some(*mark);
                    Ok(self.broadcast_state())
                }
            }
            None => Err(TttError::InvalidPlayer),
        }
    }
}
