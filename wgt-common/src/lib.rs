use std::collections::HashMap;

use serde::{de::DeserializeOwned, Deserialize, Serialize};

mod id;

pub use id::UserID;

pub trait Game: Sized + Send {
    type Error: std::error::Error + 'static;
    type Event: Clone + Serialize + DeserializeOwned;
    type Move: Send + Serialize + DeserializeOwned;

    fn new(players: HashMap<UserID, String>) -> Result<Self, Self::Error>;

    fn broadcast_state(&self) -> Vec<(Self::Event, EventReach)>;

    fn state_event_for(&self, uid: &UserID) -> Self::Event;

    fn handle_move(
        &mut self,
        mov: Self::Move,
        uid: &UserID,
    ) -> Result<Vec<(Self::Event, EventReach)>, Self::Error>;
}

#[derive(Deserialize, Serialize)]
pub enum Request<G>
where
    G: Game,
{
    OpenSession { token: Option<String> },
    Login { name: String },
    LobbyEnter { name: String },
    LobbyStart,
    GameRefresh,
    GameMove { mov: G::Move },
}

#[derive(Deserialize, Serialize)]
pub enum Response<G>
where
    G: Game,
{
    Login {
        token: Option<String>,
        name: Option<String>,
    },
    LobbyUpdate {
        name: String,
        players: Vec<String>,
    },
    GameEvent {
        event: G::Event,
    },
}

pub enum EventReach {
    Reply,        // Reply only to session that sent this
    Broadcast,    // Broadcast to every session
    Disseminate,  // Broadcast to everyone except session that sent
    User(UserID), // Send this to specific user
}
