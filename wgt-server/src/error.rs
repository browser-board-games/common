use thiserror::Error;

use crate::state::lobby::LobbyID;
use crate::state::session::MessageType;
use crate::state::session::SessionID;
use wgt_common::UserID;

pub type Result<T> = std::result::Result<T, ServerError>;

#[derive(Error, Debug)]
pub enum ServerError {
    #[error("Lobby not found {0:?}")]
    LobbyNotFound(LobbyID),
    #[error("Lobby does not have a game {0:?}")]
    LobbyNoGame(LobbyID),
    #[error("Lobby does not have valid amount of players: {0}")]
    LobbyStartInvalidPlayerCount(usize),
    #[error("Session ID {0:?} already taken")]
    SessionIDTaken(SessionID),
    #[error("Session {0:?} not found")]
    SessionNotFound(SessionID),
    #[error("Session {0:?} has no user ID attached")]
    SessionNoUserID(SessionID),
    #[error("Session {0:?} has no lobby ID attached")]
    SessionNoLobbyID(SessionID),
    #[error("Username blank")]
    UsernameBlank,
    #[error("Username too long")]
    UsernameSize,
    #[error("User ID {0:?} already taken")]
    UserIDTaken(UserID),
    #[error("User {0:?} not found")]
    UserNotFound(UserID),
    #[error("WebSocket message is not text type")]
    MessageNonText(warp::ws::Message),
    #[error("WebSocket message deserialization error")]
    MessageDeserialize(serde_json::Error),
    #[error("WebSocket message serialization error")]
    MessageSerialize(serde_json::Error),
    #[error("WebSocket message sending error")]
    MessageSend(tokio::sync::mpsc::error::SendError<MessageType>),
    #[error("Error encoding token")]
    TokenEncode(#[from] jsonwebtoken::errors::Error),
    #[error("{0}")]
    MoveError(Box<dyn std::error::Error>),
}
