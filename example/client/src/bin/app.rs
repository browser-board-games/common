#![recursion_limit = "1024"]

#[yew::function_component]
fn App() -> yew::Html {
    yew::html! {
        <wgt_yew::AppBase<tictactoe_common::TicTacToe> name={"Tic Tac Toe"}>
            <tictactoe_client::Board />
        </wgt_yew::AppBase<tictactoe_common::TicTacToe>>
    }
}

pub fn main() {
    console_error_panic_hook::set_once();
    console_log::init_with_level(log::Level::Info).expect("could not initialize logger");

    yew::Renderer::<App>::new().render();
}
