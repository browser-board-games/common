use std::collections::{hash_set::Iter, HashSet};

use wgt_common::{Game, UserID};

#[derive(Clone, Debug, Hash, Eq, PartialEq)]
pub struct LobbyID(pub String);

pub struct Lobby<G>
where
    G: Game,
{
    game: Option<G>,
    users: HashSet<UserID>,
}

impl<G: Game> Lobby<G> {
    pub fn contains_user_id(&self, user_id: &UserID) -> bool {
        self.users.contains(user_id)
    }

    pub fn iter_user_ids(&self) -> Iter<UserID> {
        self.users.iter()
    }

    pub fn add_user(&mut self, user: UserID) {
        self.users.insert(user);
    }

    pub fn get_game(&self) -> Option<&G> {
        self.game.as_ref()
    }

    pub fn get_game_mut(&mut self) -> Option<&mut G> {
        self.game.as_mut()
    }

    pub fn set_game(&mut self, game: G) {
        self.game = Some(game)
    }
}

impl<G: Game> Default for Lobby<G> {
    fn default() -> Self {
        Self {
            game: None,
            users: Default::default(),
        }
    }
}
