use std::collections::{hash_set::Iter, HashSet};

use super::session::SessionID;

pub struct User {
    name: String,
    sessions: HashSet<SessionID>,
}

impl User {
    pub(super) fn new(name: String, first_session: SessionID) -> Self {
        let mut sessions = HashSet::new();
        sessions.insert(first_session);

        Self { name, sessions }
    }

    pub(super) fn get_name(&self) -> &str {
        self.name.as_str()
    }

    pub(super) fn iter_session_ids(&self) -> Iter<SessionID> {
        self.sessions.iter()
    }

    pub(super) fn add_session(&mut self, session_id: SessionID) {
        self.sessions.insert(session_id);
    }

    pub(super) fn remove_session(&mut self, session_id: SessionID) {
        self.sessions.remove(&session_id);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_user_new() {
        let session_id = SessionID::new();
        let user = User::new("player".to_string(), session_id);

        assert_eq!("player".to_string(), user.name);
        assert_eq!("player", user.get_name());
        assert!(user.sessions.contains(&session_id));
        assert_eq!(1, user.sessions.len());
    }

    #[test]
    fn test_user_sessions() {
        let session_id_1 = SessionID::new();
        let session_id_2 = SessionID::new();

        let mut user = User::new("player".to_string(), session_id_2);
        assert_eq!(
            vec![&session_id_2],
            user.iter_session_ids().collect::<Vec<_>>()
        );

        user.add_session(session_id_1);
        assert_eq!(
            {
                let mut set = HashSet::new();
                set.insert(&session_id_1);
                set.insert(&session_id_2);
                set
            },
            user.iter_session_ids().collect::<HashSet<_>>()
        );

        user.add_session(session_id_2);
        assert_eq!(
            {
                let mut set = HashSet::new();
                set.insert(&session_id_1);
                set.insert(&session_id_2);
                set
            },
            user.iter_session_ids().collect::<HashSet<_>>()
        );

        user.remove_session(session_id_2);
        assert_eq!(
            vec![&session_id_1],
            user.iter_session_ids().collect::<Vec<_>>()
        );
    }
}
