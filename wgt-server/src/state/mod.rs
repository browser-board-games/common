use std::collections::HashMap;

use chrono::{Months, Utc};
use jsonwebtoken::{decode, encode, DecodingKey, EncodingKey, Header, Validation};
use log::*;
use serde::{Deserialize, Serialize};
use warp::ws::Message;

pub(crate) mod lobby;
pub(crate) mod session;
pub(crate) mod user;

use self::lobby::{Lobby, LobbyID};
use self::session::{Sender, Session, SessionID};
use self::user::User;
use crate::error::{Result, ServerError};
use wgt_common::{EventReach, Game, Request, Response, UserID};

#[derive(Debug, Deserialize, Serialize)]
struct SessionClaims {
    exp: i64,
    user_id: UserID,
}

pub struct ServerState<G>
where
    G: Game,
{
    token_key: Vec<u8>,
    users: HashMap<UserID, User>,
    sessions: HashMap<SessionID, Session>,
    lobbies: HashMap<LobbyID, Lobby<G>>,
}

impl<G: Game> ServerState<G> {
    pub fn new(token_key: Vec<u8>) -> Self {
        Self {
            token_key,
            users: Default::default(),
            sessions: Default::default(),
            lobbies: Default::default(),
        }
    }

    fn encode_token(&self, user_id: UserID) -> Result<String> {
        let claims = SessionClaims {
            exp: (Utc::now() + Months::new(120)).timestamp(),
            user_id,
        };
        let token = encode(
            &Header::default(),
            &claims,
            &EncodingKey::from_secret(self.token_key.as_ref()),
        )?;
        Ok(token)
    }

    fn decode_token(&self, cookie: String) -> Result<UserID> {
        let token = decode::<SessionClaims>(
            &cookie,
            &DecodingKey::from_secret(self.token_key.as_ref()),
            &Validation::default(),
        )?;
        Ok(token.claims.user_id)
    }

    pub(crate) fn add_session(&mut self, session_id: SessionID, sender: Sender) -> Result<()> {
        if self.sessions.contains_key(&session_id) {
            return Err(ServerError::SessionIDTaken(session_id));
        }

        self.sessions.insert(session_id, Session::new(sender));
        Ok(())
    }

    pub(crate) fn remove_session(&mut self, session_id: SessionID) {
        self.sessions.remove(&session_id);
        for (_, user) in self.users.iter_mut() {
            user.remove_session(session_id);
        }
    }

    fn register_user(
        &mut self,
        user_id: UserID,
        name: String,
        session_id: SessionID,
    ) -> Result<()> {
        let session = self
            .sessions
            .get_mut(&session_id)
            .ok_or_else(|| ServerError::SessionNotFound(session_id))?;
        if self.users.contains_key(&user_id) {
            return Err(ServerError::UserIDTaken(user_id));
        }
        let trimmed = name.trim();
        if trimmed.is_empty() {
            return Err(ServerError::UsernameBlank);
        } else if trimmed.len() > 255 {
            return Err(ServerError::UsernameSize);
        }

        session.set_user_id(user_id);
        self.users
            .insert(user_id, User::new(trimmed.to_string(), session_id));

        Ok(())
    }

    fn user_add_session(&mut self, user_id: UserID, session_id: SessionID) -> Result<()> {
        let session = self
            .sessions
            .get_mut(&session_id)
            .ok_or_else(|| ServerError::SessionNotFound(session_id))?;
        let user = self
            .users
            .get_mut(&user_id)
            .ok_or_else(|| ServerError::UserNotFound(user_id))?;

        session.set_user_id(user_id);
        user.add_session(session_id);

        Ok(())
    }

    fn send_session(&self, session_id: SessionID, message: &Response<G>) -> Result<()> {
        let session = self
            .sessions
            .get(&session_id)
            .ok_or_else(|| ServerError::SessionNotFound(session_id))?;

        serde_json::to_string(&message)
            .map_err(ServerError::MessageSerialize)
            .and_then(|text| {
                session
                    .send(Message::text(text))
                    .map_err(ServerError::MessageSend)
            })
    }

    pub(crate) fn ping_session(&self, session_id: SessionID) -> Result<()> {
        let session = self
            .sessions
            .get(&session_id)
            .ok_or_else(|| ServerError::SessionNotFound(session_id))?;

        session
            .send(Message::ping(vec![13, 37]))
            .map_err(ServerError::MessageSend)
    }

    fn send_user(&self, user_id: UserID, message: &Response<G>) -> Result<()> {
        let user = self
            .users
            .get(&user_id)
            .ok_or_else(|| ServerError::UserNotFound(user_id))?;

        for session_id in user.iter_session_ids() {
            // We do not want this method to fail, so just log the errors
            if let Err(e) = self.send_session(*session_id, message) {
                error!("{}", e);
            }
        }

        Ok(())
    }

    pub(crate) fn handle_message(
        &mut self,
        session_id: SessionID,
        message: Request<G>,
    ) -> Result<()> {
        match message {
            Request::OpenSession { token } => match self.decode_token(token.unwrap_or_default()) {
                Ok(user_id) => {
                    self.user_add_session(user_id, session_id)?;

                    self.send_session(
                        session_id,
                        &Response::Login {
                            token: None,
                            name: self.users.get(&user_id).map(User::get_name).map(Into::into),
                        },
                    )
                }
                Err(_) => self.send_session(
                    session_id,
                    &Response::Login {
                        token: None,
                        name: None,
                    },
                ),
            },
            Request::Login { name } => {
                let user_id = UserID::new();

                self.register_user(user_id, name.clone(), session_id)?;

                self.send_session(
                    session_id,
                    &Response::Login {
                        token: Some(self.encode_token(user_id)?),
                        name: Some(name),
                    },
                )
            }
            Request::LobbyEnter { name } => self.join_lobby(session_id, name),
            Request::LobbyStart => self
                .sessions
                .get(&session_id)
                .ok_or_else(|| ServerError::SessionNotFound(session_id))
                .and_then(|session| {
                    session
                        .get_lobby_id()
                        .cloned()
                        .ok_or_else(|| ServerError::SessionNoLobbyID(session_id))
                })
                .and_then(|lid| self.start_lobby(lid)),
            Request::GameRefresh => self
                .sessions
                .get(&session_id)
                .ok_or_else(|| ServerError::SessionNotFound(session_id))
                .and_then(|session| {
                    session
                        .get_lobby_id()
                        .cloned()
                        .ok_or_else(|| ServerError::SessionNoLobbyID(session_id))
                })
                .and_then(|lid| self.game_refresh(session_id, lid)),
            Request::GameMove { mov } => self
                .sessions
                .get(&session_id)
                .ok_or_else(|| ServerError::SessionNotFound(session_id))
                .and_then(|session| {
                    session
                        .get_lobby_id()
                        .cloned()
                        .ok_or_else(|| ServerError::SessionNoLobbyID(session_id))
                })
                .and_then(|lid| self.game_move(session_id, lid, mov)),
        }
    }

    fn join_lobby(&mut self, session_id: SessionID, name: String) -> Result<()> {
        let user_id = self
            .sessions
            .get(&session_id)
            .ok_or_else(|| ServerError::SessionNotFound(session_id))
            .and_then(|session| {
                session
                    .get_user_id()
                    .cloned()
                    .ok_or_else(|| ServerError::SessionNoUserID(session_id))
            })?;

        let lobby = self
            .lobbies
            .entry(LobbyID(name.clone()))
            .or_insert_with(Default::default);
        if !lobby.contains_user_id(&user_id) && lobby.get_game().is_none() {
            lobby.add_user(user_id);
        }

        match self.sessions.get_mut(&session_id) {
            Some(session) => session.set_lobby_id(LobbyID(name.clone())),
            None => return Err(ServerError::SessionNotFound(session_id)),
        }

        let lobby = self
            .lobbies
            .get(&LobbyID(name.clone()))
            .ok_or_else(|| ServerError::LobbyNotFound(LobbyID(name.clone())))?;

        let lobby_update = Response::LobbyUpdate {
            name,
            players: lobby
                .iter_user_ids()
                .filter_map(|uid| self.users.get(uid).map(User::get_name).map(Into::into))
                .collect::<Vec<_>>(),
        };
        for &uid in lobby.iter_user_ids() {
            // We do not want this method to fail, so just log the errors
            if let Err(e) = self.send_user(uid, &lobby_update) {
                error!("{}", e);
            }
        }
        if let Some(game) = lobby.get_game() {
            self.send_user(
                user_id,
                &Response::GameEvent {
                    event: game.state_event_for(&user_id),
                },
            )?;
        }

        Ok(())
    }

    fn start_lobby(&mut self, name: LobbyID) -> Result<()> {
        let players: HashMap<UserID, String> = match self.lobbies.get(&name) {
            Some(lobby) => lobby
                .iter_user_ids()
                .filter_map(|uid| {
                    self.users
                        .get(uid)
                        .map(|u| (*uid, u.get_name().to_string()))
                })
                .collect(),
            None => return Err(ServerError::LobbyNotFound(name.clone())),
        };

        if players.len() < 2 || players.len() > 10 {
            return Err(ServerError::LobbyStartInvalidPlayerCount(players.len()));
        }

        match self.lobbies.get_mut(&name) {
            Some(lobby) => {
                lobby.set_game(G::new(players).map_err(|e| ServerError::MoveError(Box::new(e)))?)
            }
            None => return Err(ServerError::LobbyNotFound(name.clone())),
        }

        let lobby = self
            .lobbies
            .get(&name)
            .ok_or_else(|| ServerError::LobbyNotFound(name.clone()))?;
        let game = lobby
            .get_game()
            .ok_or_else(|| ServerError::LobbyNoGame(name))?;

        for &uid in lobby.iter_user_ids() {
            // We do not want this method to fail, so just log all the errors
            let event = game.state_event_for(&uid);
            if let Err(e) = self.send_user(uid, &Response::GameEvent { event }) {
                error!("{}", e);
            }
        }

        Ok(())
    }

    fn game_refresh(&mut self, session_id: SessionID, name: LobbyID) -> Result<()> {
        let user_id = self
            .sessions
            .get(&session_id)
            .ok_or_else(|| ServerError::SessionNotFound(session_id))
            .and_then(|session| {
                session
                    .get_user_id()
                    .ok_or_else(|| ServerError::SessionNoUserID(session_id))
            })?;
        let game = self
            .lobbies
            .get(&name)
            .ok_or_else(|| ServerError::LobbyNotFound(name.clone()))?
            .get_game()
            .ok_or_else(|| ServerError::LobbyNoGame(name))?;
        self.send_session(
            session_id,
            &Response::GameEvent {
                event: game.state_event_for(user_id),
            },
        )
    }

    fn game_move(&mut self, session_id: SessionID, name: LobbyID, mov: G::Move) -> Result<()> {
        let user_id = self
            .sessions
            .get(&session_id)
            .ok_or_else(|| ServerError::SessionNotFound(session_id))
            .and_then(|session| {
                session
                    .get_user_id()
                    .ok_or_else(|| ServerError::SessionNoUserID(session_id))
            })?;

        let events = match self.lobbies.get_mut(&name) {
            Some(lobby) => match lobby.get_game_mut() {
                Some(game) => game
                    .handle_move(mov, user_id)
                    .map_err(|e| ServerError::MoveError(Box::new(e))),
                None => return Err(ServerError::LobbyNoGame(name)),
            },
            None => return Err(ServerError::LobbyNotFound(name)),
        }?;
        let lobby = match self.lobbies.get(&name) {
            Some(lobby) => lobby,
            None => return Err(ServerError::LobbyNotFound(name)),
        };

        for (event, reach) in events.into_iter() {
            match reach {
                EventReach::Reply => {
                    match self.send_session(session_id, &Response::GameEvent { event }) {
                        Ok(_) => {}
                        Err(e) => error!("{}", e),
                    }
                }
                EventReach::Broadcast => {
                    for &uid in lobby.iter_user_ids() {
                        match self.send_user(
                            uid,
                            &Response::GameEvent {
                                event: event.clone(),
                            },
                        ) {
                            Ok(_) => {}
                            Err(e) => error!("{}", e),
                        }
                    }
                }
                EventReach::Disseminate => {
                    for &uid in lobby.iter_user_ids() {
                        match self.users.get(&uid) {
                            Some(user) => {
                                for &sid in user.iter_session_ids() {
                                    if sid != session_id {
                                        match self.send_session(
                                            sid,
                                            &Response::GameEvent {
                                                event: event.clone(),
                                            },
                                        ) {
                                            Ok(_) => {}
                                            Err(e) => error!("{}", e),
                                        }
                                    }
                                }
                            }
                            None => error!("{}", ServerError::UserNotFound(uid)),
                        }
                    }
                }
                EventReach::User(id) => match self.users.iter().find(|(k, _)| k == &&id) {
                    Some((uid, _)) => match self.send_user(*uid, &Response::GameEvent { event }) {
                        Ok(_) => {}
                        Err(e) => error!("{}", e),
                    },
                    None => error!("User {:?} not found", id),
                },
            }
        }

        Ok(())
    }
}

// #[cfg(test)]
// mod tests {
//     use tokio::sync::mpsc;

//     use super::id::{new_session_id, new_user_id};
//     use super::session::MessageType;
//     use super::*;

//     fn timeout<T>(future: T) -> tokio::time::Timeout<T>
//     where
//         T: futures::Future,
//     {
//         tokio::time::timeout(tokio::time::Duration::from_millis(100), future)
//     }

//     fn decode(message: Message) -> Response {
//         serde_json::from_str(message.to_str().unwrap()).unwrap()
//     }

//     async fn receive(rx: &mut tokio::sync::mpsc::UnboundedReceiver<MessageType>) -> Response {
//         decode(timeout(rx.recv()).await.unwrap().unwrap().unwrap())
//     }

//     #[test]
//     fn test_state_new() {
//         let state = State::new("abc".to_string());

//         assert_eq!("abc".to_string(), state.token_key);
//         assert!(state.sessions.is_empty());
//         assert!(state.users.is_empty());
//         assert!(state.lobbies.is_empty());
//     }

//     #[test]
//     fn test_token_encode_decode() {
//         let state = State::new("secret".to_string());

//         let user_id = new_user_id();
//         let encoded = state.encode_token(user_id).unwrap();
//         let decoded = state.decode_token(encoded).unwrap();
//         assert_eq!(user_id, decoded);
//     }

//     #[tokio::test]
//     async fn test_state_sessions() {
//         let mut state = State::new("secret".to_string());

//         let session_id_1 = new_session_id();
//         let (tx_1, mut rx_1) = mpsc::unbounded_channel::<MessageType>();
//         let session_id_2 = new_session_id();
//         let (tx_2, mut rx_2) = mpsc::unbounded_channel::<MessageType>();

//         state.add_session(session_id_1, tx_1).unwrap();
//         match state.add_session(session_id_1, tx_2.clone()) {
//             Err(Error::SessionIDTaken(id)) => assert_eq!(session_id_1, id),
//             _ => unreachable!(),
//         }
//         match state.send_session(
//             session_id_2,
//             &Response::Login {
//                 token: None,
//                 name: None,
//             },
//         ) {
//             Err(Error::SessionNotFound(id)) => assert_eq!(session_id_2, id),
//             _ => unreachable!(),
//         }

//         state.add_session(session_id_2, tx_2).unwrap();
//         state
//             .send_session(
//                 session_id_2,
//                 &Response::Login {
//                     token: None,
//                     name: None,
//                 },
//             )
//             .unwrap();
//         assert_eq!(
//             Response::Login {
//                 token: None,
//                 name: None
//             },
//             receive(&mut rx_2).await
//         );
//         assert!(timeout(rx_1.recv()).await.is_err());

//         state.remove_session(session_id_2);
//         match state.send_session(
//             session_id_2,
//             &Response::Login {
//                 token: None,
//                 name: None,
//             },
//         ) {
//             Err(Error::SessionNotFound(id)) => assert_eq!(session_id_2, id),
//             _ => unreachable!(),
//         }

//         state
//             .send_session(
//                 session_id_1,
//                 &Response::Login {
//                     token: None,
//                     name: None,
//                 },
//             )
//             .unwrap();
//         assert_eq!(
//             Response::Login {
//                 token: None,
//                 name: None
//             },
//             receive(&mut rx_1).await
//         );
//     }

//     #[tokio::test]
//     async fn test_state_users() {
//         let mut state = State::new("secret".to_string());
//         let message = Response::Login {
//             token: None,
//             name: None,
//         };

//         let user_id_1 = new_user_id();
//         let user_id_2 = new_user_id();

//         let session_id_1 = new_session_id();
//         let (tx_1, mut rx_1) = mpsc::unbounded_channel::<MessageType>();
//         let session_id_2 = new_session_id();
//         let (tx_2, mut rx_2) = mpsc::unbounded_channel::<MessageType>();
//         let session_id_3 = new_session_id();
//         let (tx_3, mut rx_3) = mpsc::unbounded_channel::<MessageType>();

//         state.add_session(session_id_1, tx_1).unwrap();
//         match state.user_add_session(user_id_1, session_id_1) {
//             Err(Error::UserNotFound(id)) => assert_eq!(user_id_1, id),
//             _ => unreachable!(),
//         };

//         state
//             .register_user(user_id_1, "player".to_string(), session_id_1)
//             .unwrap();
//         // Double add should work
//         state.user_add_session(user_id_1, session_id_1).unwrap();
//         match state.user_add_session(user_id_1, session_id_2) {
//             Err(Error::SessionNotFound(id)) => assert_eq!(session_id_2, id),
//             _ => unreachable!(),
//         };

//         state.add_session(session_id_2, tx_2).unwrap();
//         match state.register_user(user_id_1, "player".to_string(), session_id_2) {
//             Err(Error::UserIDTaken(id)) => assert_eq!(user_id_1, id),
//             _ => unreachable!(),
//         }
//         state.user_add_session(user_id_1, session_id_2).unwrap();

//         state.add_session(session_id_3, tx_3).unwrap();
//         match state.send_user(user_id_2, &message) {
//             Err(Error::UserNotFound(id)) => assert_eq!(user_id_2, id),
//             _ => unreachable!(),
//         }
//         state
//             .register_user(user_id_2, "player".to_string(), session_id_3)
//             .unwrap();

//         state.send_user(user_id_1, &message).unwrap();
//         state.send_user(user_id_2, &message).unwrap();
//         assert_eq!(message, receive(&mut rx_1).await);
//         assert_eq!(message, receive(&mut rx_2).await);
//         assert_eq!(message, receive(&mut rx_3).await);

//         state.sessions.remove(&session_id_2);
//         state.send_user(user_id_1, &message).unwrap();
//     }

//     #[test]
//     fn test_username_size() {
//         let mut state = State::new("secret".to_string());

//         let user_id = new_user_id();

//         let session_id = new_session_id();
//         let (tx, _) = mpsc::unbounded_channel::<MessageType>();

//         state.add_session(session_id, tx).unwrap();
//         match state.register_user(user_id, " \t".to_string(), session_id) {
//             Err(Error::UsernameBlank) => {}
//             _ => unreachable!(),
//         }
//         match state.register_user(
//             user_id,
//             std::iter::repeat("a").take(300).collect::<String>(),
//             session_id,
//         ) {
//             Err(Error::UsernameSize) => {}
//             _ => unreachable!(),
//         }
//         state
//             .register_user(user_id, "Player Name".to_string(), session_id)
//             .unwrap();
//     }
// }
