use std::sync::atomic::{AtomicUsize, Ordering};

use serde::{Deserialize, Serialize};
use tokio::sync::mpsc;
use warp::ws::Message;

use super::lobby::LobbyID;
use wgt_common::UserID;

static NEXT_SESSION_ID: AtomicUsize = AtomicUsize::new(1);

#[derive(Clone, Copy, Debug, Deserialize, Eq, Hash, PartialEq, Serialize)]
pub struct SessionID(usize);

impl SessionID {
    pub fn new() -> Self {
        SessionID(NEXT_SESSION_ID.fetch_add(1, Ordering::Relaxed))
    }
}

pub type MessageType = Result<Message, warp::Error>;
pub type Sender = mpsc::UnboundedSender<MessageType>;

#[derive(Debug)]
pub struct Session {
    user_id: Option<UserID>,
    lobby_id: Option<LobbyID>,
    sender: Sender,
}

impl Session {
    pub(super) fn new(sender: Sender) -> Self {
        Self {
            user_id: None,
            lobby_id: None,
            sender,
        }
    }

    pub(super) fn send(&self, message: Message) -> Result<(), mpsc::error::SendError<MessageType>> {
        self.sender.send(Ok(message))
    }

    pub(super) fn get_user_id(&self) -> Option<&UserID> {
        self.user_id.as_ref()
    }

    pub(super) fn set_user_id(&mut self, user_id: UserID) {
        self.user_id = Some(user_id)
    }

    pub(super) fn get_lobby_id(&self) -> Option<&LobbyID> {
        self.lobby_id.as_ref()
    }

    pub(super) fn set_lobby_id(&mut self, lobby_id: LobbyID) {
        self.lobby_id = Some(lobby_id)
    }
}

#[cfg(test)]
mod tests {
    use tokio::time::{timeout, Duration};

    use super::*;

    #[tokio::test]
    async fn test_session_new() {
        let (tx, mut rx) = mpsc::unbounded_channel::<Result<Message, warp::Error>>();
        let session = Session::new(tx);
        session.sender.send(Ok(Message::text("test1"))).unwrap();
        session.send(Message::text("test2")).unwrap();

        assert_eq!(
            "test1",
            timeout(Duration::from_millis(100), rx.recv())
                .await
                .unwrap()
                .unwrap()
                .unwrap()
                .to_str()
                .unwrap()
        );
        assert_eq!(
            "test2",
            timeout(Duration::from_millis(100), rx.recv())
                .await
                .unwrap()
                .unwrap()
                .unwrap()
                .to_str()
                .unwrap()
        );
        assert_eq!(None, session.user_id);
    }

    #[test]
    fn test_next_session_id() {
        let session_id_1 = SessionID::new();
        let session_id_2 = SessionID::new();
        let session_id_3 = SessionID::new();

        assert_ne!(session_id_1, session_id_2);
        assert_ne!(session_id_2, session_id_3);
    }

    #[test]
    fn test_session_user_id() {
        let (tx, _) = mpsc::unbounded_channel::<Result<Message, warp::Error>>();
        let mut session = Session::new(tx);

        assert_eq!(None, session.user_id);
        assert_eq!(None, session.get_user_id());

        let user_id = UserID::new();
        session.set_user_id(user_id);
        assert_eq!(Some(user_id), session.user_id);
        assert_eq!(Some(user_id).as_ref(), session.get_user_id());
    }
}
