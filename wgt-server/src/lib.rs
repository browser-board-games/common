use std::net::SocketAddr;
use std::sync::Arc;

use clap::Parser;
use rand::{distributions::Alphanumeric, thread_rng, Rng};
use tokio::sync::Mutex;
use warp::Filter;

mod error;
mod state;
mod websocket;

use crate::state::ServerState;
use wgt_common::Game;

#[derive(Debug, Parser)]
#[command(author, version, about, long_about=None)]
pub struct Args {
    /// Bind to the specified address
    #[arg(short, long, env, default_value = "127.0.0.1:8000")]
    listen: String,

    /// Set the URL path prefix
    #[arg(short, long, env)]
    prefix: Option<String>,

    /// Set the log level verbosity
    #[arg(short, long, env, value_parser = ["error", "warning", "info", "debug", "trace"], default_value = "info")]
    verbosity: String,
}

#[tokio::main]
pub async fn serve<G: Game + 'static>() {
    let args = Args::parse();

    std::env::set_var("RUST_LOG", args.verbosity);
    pretty_env_logger::init();

    let prefix = match args.prefix {
        Some(prefix) => warp::path(prefix.to_string()).boxed(),
        None => warp::any().boxed(),
    };

    let state = Arc::new(Mutex::new(ServerState::<G>::new(
        thread_rng().sample_iter(&Alphanumeric).take(32).collect(),
    )));
    let state = warp::any().map(move || state.clone());

    let ws = prefix
        .clone()
        .and(warp::path!("ws" / "v1"))
        .and(warp::ws())
        .and(state)
        .map(|ws: warp::ws::Ws, state| {
            ws.on_upgrade(move |socket| websocket::user_connected(socket, state))
        });
    let statics = prefix.and(warp::fs::dir("client/dist"));
    let routes = ws.or(statics);

    let address: SocketAddr = args.listen.parse().unwrap();
    warp::serve(routes).run(address).await;
}
