use wasm_bindgen::UnwrapThrowExt;
use wgt_yew::{AppBase, AppBaseMsg, Parent, Sound};
use yew::prelude::*;

use tictactoe_common::{TicTacToe, TttError, TttEvent, TttMove, BOARD_SIZE};

pub struct Board {
    parent: Parent<TicTacToe>,

    game_error: Option<TttError>,
    game_state: Option<TicTacToe>,
    game_winner: Option<Option<String>>,
}

pub enum BoardMsg {
    RegisterParent(Parent<TicTacToe>),
    GameEvent(TttEvent),
    Place(usize),
}

impl Component for Board {
    type Message = BoardMsg;
    type Properties = ();

    fn create(_: &Context<Self>) -> Self {
        Self {
            parent: Default::default(),

            game_error: None,
            game_state: None,
            game_winner: None,
        }
    }

    fn update(&mut self, _: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            BoardMsg::RegisterParent(parent) => {
                self.parent = parent;
                false
            }
            BoardMsg::GameEvent(event) => {
                self.game_error.take();
                match event {
                    TttEvent::Error { error } => {
                        self.game_error = Some(error);
                    }
                    TttEvent::State { state } => {
                        if let Some(old) = &self.game_state {
                            if old.turn().unwrap_throw() != state.turn().unwrap_throw()
                                && state
                                    .players
                                    .get(state.turn().unwrap_throw())
                                    .unwrap_throw()
                                    .0
                                    == self.parent.username
                            {
                                self.parent.sound.emit(Sound::Notify);
                            }
                        }
                        self.game_state = Some(state);
                    }
                    TttEvent::End { winner } => {
                        self.game_winner = Some(winner);
                    }
                }
                true
            }
            BoardMsg::Place(pos) => {
                self.parent.mov.emit(TttMove(pos));
                false
            }
        }
    }

    fn rendered(&mut self, ctx: &Context<Self>, first_render: bool) {
        if first_render {
            ctx.link()
                .get_parent()
                .expect("No parent found")
                .downcast::<AppBase<TicTacToe>>()
                .send_message(AppBaseMsg::RegisterBoard {
                    down: ctx.link().callback(BoardMsg::GameEvent),
                    up: ctx.link().callback(BoardMsg::RegisterParent),
                });
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        html! {
            <>
                if let Some(error) = &self.game_error {
                    <p>{ format!("Error: {}", error) }</p>
                }
                if let Some(winner) = &self.game_winner {
                    <p>
                        if let Some(winner) = winner {
                            { format!("{} has won the game!", winner) }
                        } else {
                            { format!("Game ended in tie!") }
                        }
                    </p>
                }
                if let Some(state) = &self.game_state {
                    <div class="board">
                        {
                            (0..BOARD_SIZE).map(|pos| {
                                match state.board[pos] {
                                    Some(mark) => if mark {
                                        html! { <span>{ "X" }</span> }
                                    } else {
                                        html! { <span>{ "O" }</span> }
                                    },
                                    None => html! {
                                        <button
                                            onclick={ctx.link().callback(move |_| BoardMsg::Place(pos))}
                                        >
                                            { " " }
                                        </button>
                                    }
                                }
                            }).collect::<Html>()
                        }
                    </div>
                }
            </>
        }
    }
}
