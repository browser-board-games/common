use std::sync::Arc;

use futures_util::{FutureExt, StreamExt};
use log::*;
use tokio::sync::{mpsc, Mutex};
use tokio::time::{self, Duration};
use tokio_stream::wrappers::UnboundedReceiverStream;
use warp::ws::{Message, WebSocket};

use crate::error::{Result, ServerError};
use crate::state::{session::SessionID, ServerState};
use wgt_common::{Game, Request};

type StateRef<G> = Arc<Mutex<ServerState<G>>>;

pub async fn user_connected<G: Game + 'static>(ws: WebSocket, state: StateRef<G>) {
    let session_id = SessionID::new();

    info!(
        "New websocket connection, session: {:?}, ws: {:?}",
        session_id, ws
    );

    let (session_ws_tx, mut session_ws_rx) = ws.split();

    let (tx, rx) = mpsc::unbounded_channel();
    let rx = UnboundedReceiverStream::new(rx);
    tokio::task::spawn(rx.forward(session_ws_tx).map(move |result| {
        if let Err(e) = result {
            error!("WebSocket receive error (sid={:?}): {}", session_id, e);
        }
    }));

    if let Err(e) = state.lock().await.add_session(session_id, tx) {
        error!("Error registering session: {}", e);
        return;
    };

    tokio::task::spawn(session_keepalive(session_id, state.clone()));

    while let Some(result) = session_ws_rx.next().await {
        let msg = match result {
            Ok(msg) => msg,
            Err(e) => {
                warn!("WebSocket receive error (sid={:?}): {}", session_id, e);
                break;
            }
        };
        if let Err(e) = handle_message(msg, session_id, state.clone()).await {
            error!("{}", e);
        }
    }

    user_disconnected(session_id, state).await;
}

async fn session_keepalive<G: Game>(session_id: SessionID, state: StateRef<G>) {
    let mut interval = time::interval(Duration::from_secs(30));

    loop {
        interval.tick().await;

        if let Err(e) = state.lock().await.ping_session(session_id) {
            error!("Error during keepalive: {}", e);
            break;
        }
    }
}

async fn user_disconnected<G: Game>(session_id: SessionID, state: StateRef<G>) {
    info!("Session disconnected: {:?}", session_id);

    state.lock().await.remove_session(session_id);
}

async fn handle_message<G: Game>(
    message: Message,
    session_id: SessionID,
    state: StateRef<G>,
) -> Result<()> {
    if message.is_ping() || message.is_pong() {
        // We don't need to handle ping or pong messages
        return Ok(());
    }

    let message_text: &str = message
        .to_str()
        .map_err(|_| ServerError::MessageNonText(message.clone()))?;
    let message: Request<G> =
        serde_json::from_str(message_text).map_err(ServerError::MessageDeserialize)?;

    let mut state = state.lock().await;

    state.handle_message(session_id, message)
}
