use std::sync::atomic::{AtomicUsize, Ordering};

use serde::{Deserialize, Serialize};

static NEXT_USER_ID: AtomicUsize = AtomicUsize::new(1);

#[derive(Clone, Copy, Debug, Deserialize, Eq, Hash, PartialEq, Serialize)]
pub struct UserID(usize);

impl UserID {
    #[allow(clippy::new_without_default)]
    pub fn new() -> Self {
        UserID(NEXT_USER_ID.fetch_add(1, Ordering::Relaxed))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_next_user_id() {
        let user_id_1 = UserID::new();
        let user_id_2 = UserID::new();
        let user_id_3 = UserID::new();

        assert_ne!(user_id_1, user_id_2);
        assert_ne!(user_id_2, user_id_3);
    }
}
