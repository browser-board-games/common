use futures::channel::mpsc::{channel, Sender};
use futures_util::{SinkExt, StreamExt};
use gloo_events::{EventListener, EventListenerOptions};
use gloo_net::websocket::{futures::WebSocket, Message, WebSocketError};
use gloo_utils::window;
use js_sys::Uint8Array;
use wasm_bindgen::{JsCast, UnwrapThrowExt};
use wasm_bindgen_futures::spawn_local;
use wasm_bindgen_futures::JsFuture;
use web_sys::{
    AudioBuffer, AudioContext, BeforeUnloadEvent, HtmlFormElement, HtmlInputElement, SubmitEvent,
};
use yew::prelude::*;

use wgt_common::{Game, Request, Response};

const SOUND_NOTIFICATION: &[u8] = include_bytes!("../assets/notification.ogg");

pub struct AppBase<G: Game> {
    ws: Sender<Request<G>>,
    ws_connected: bool,
    route: Route,
    board: Option<Callback<G::Event>>,

    username: Option<String>,
    lobby_data: Option<(String, Vec<String>)>,

    audio_context: Option<AudioContext>,
    notification: Option<AudioBuffer>,

    _listener: EventListener,
}

pub struct Parent<G: Game> {
    pub username: String,
    pub mov: Callback<G::Move>,
    pub sound: Callback<Sound>,
}

pub enum Sound {
    Notify,
}

enum Route {
    Splash,
    Login,
    Lobby,
    Game,
}

pub enum AppBaseMsg<G: Game> {
    BeforeUnloadEvent(BeforeUnloadEvent),
    RegisterBoard {
        down: Callback<G::Event>,
        up: Callback<Parent<G>>,
    },
    AudioDecoded(AudioBuffer),
    PlaySound(Sound),
    WebSocket(Result<Message, WebSocketError>),
    Reload,
    LoginSubmit(SubmitEvent),
    LobbyCodeSubmit(SubmitEvent),
    LobbyStart,
    GameMove(G::Move),
}

#[derive(PartialEq, Properties)]
pub struct AppBaseProps {
    #[prop_or_default]
    pub children: Children,
    pub name: String,
}

impl<G: Game + 'static> Component for AppBase<G> {
    type Message = AppBaseMsg<G>;
    type Properties = AppBaseProps;

    fn create(ctx: &Context<Self>) -> Self {
        let cb = ctx.link().callback(AppBaseMsg::BeforeUnloadEvent);
        let _listener = EventListener::new_with_options(
            &window(),
            "beforeunload",
            EventListenerOptions::enable_prevent_default(),
            move |ev| cb.emit(ev.clone().dyn_into::<BeforeUnloadEvent>().unwrap_throw()),
        );

        let audio_context = AudioContext::new().ok();
        if let Some(audio_context) = audio_context.clone() {
            ctx.link().send_future(async move {
                let buffer: Uint8Array = SOUND_NOTIFICATION.into();
                let buffer = JsFuture::from(
                    audio_context
                        .decode_audio_data(&buffer.buffer())
                        .unwrap_throw(),
                )
                .await
                .unwrap_throw()
                .dyn_into::<AudioBuffer>()
                .unwrap_throw();
                AppBaseMsg::AudioDecoded(buffer)
            });
        }

        let location = window().location();
        let url = format!(
            "{}//{}{}ws/v1",
            if location.protocol().unwrap_throw() == "http:" {
                "ws:"
            } else {
                "wss:"
            },
            location.host().unwrap_throw(),
            location.pathname().unwrap_throw(),
        );
        let ws = WebSocket::open(&url).unwrap_throw();
        let (mut tx, rx) = ws.split();

        ctx.link().send_stream(rx.map(AppBaseMsg::WebSocket));
        let (ws, mut ws_rx) = channel::<Request<G>>(1000);
        spawn_local(async move {
            while let Some(msg) = ws_rx.next().await {
                let msg = serde_json::to_string(&msg).expect("could not serialize");
                tx.send(Message::Text(msg)).await.unwrap_throw();
            }
        });

        Self {
            ws,
            ws_connected: true,
            route: Route::Splash,
            board: None,

            username: None,
            lobby_data: None,

            audio_context,
            notification: None,

            _listener,
        }
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            AppBaseMsg::BeforeUnloadEvent(event) => {
                if matches!(self.route, Route::Game) {
                    event.prevent_default();
                    event.set_return_value(""); // Chrome requires this
                }
                false
            }
            AppBaseMsg::RegisterBoard { down, up } => {
                self.board = Some(down);
                up.emit(Parent {
                    username: self.username.clone().unwrap_or_default(),
                    mov: ctx.link().callback(AppBaseMsg::GameMove),
                    sound: ctx.link().callback(AppBaseMsg::PlaySound),
                });
                self.ws.try_send(Request::GameRefresh).unwrap_throw();
                false
            }
            AppBaseMsg::AudioDecoded(audio) => {
                self.notification = Some(audio);
                false
            }
            AppBaseMsg::PlaySound(sound) => {
                match sound {
                    Sound::Notify => {
                        if let Some(audio_context) = self.audio_context.clone() {
                            if let Ok(source_node) = audio_context.create_buffer_source() {
                                if let Some(notification) = self.notification.clone() {
                                    source_node.set_buffer(Some(&notification));
                                    source_node
                                        .connect_with_audio_node(&audio_context.destination())
                                        .ok();
                                    source_node.start().ok();
                                }
                            }
                        }
                    }
                }
                false
            }
            AppBaseMsg::WebSocket(res) => match res {
                Ok(msg) => match msg {
                    Message::Text(txt) => match serde_json::from_str(&txt) {
                        Ok(msg) => match msg {
                            Response::Login { token, name } => {
                                if let Some(token) = token {
                                    let local_storage =
                                        window().local_storage().unwrap_throw().unwrap_throw();
                                    local_storage
                                        .set(&session_token_name(&ctx.props().name), &token)
                                        .unwrap_throw();
                                }
                                self.route = match name {
                                    Some(_) => Route::Lobby,
                                    None => Route::Login,
                                };
                                self.username = name;
                                true
                            }
                            Response::LobbyUpdate { name, players } => {
                                self.lobby_data = Some((name, players));
                                true
                            }
                            Response::<G>::GameEvent { event } => {
                                if let Some(board) = &self.board {
                                    board.emit(event);
                                }
                                if matches!(self.route, Route::Lobby) {
                                    self.route = Route::Game;
                                    true
                                } else {
                                    false
                                }
                            }
                        },
                        Err(_) => false,
                    },
                    Message::Bytes(_) => false,
                },
                Err(_) => {
                    self.ws_connected = false;
                    true
                }
            },

            AppBaseMsg::Reload => {
                window().location().reload().unwrap_throw();
                false
            }
            AppBaseMsg::LoginSubmit(e) => {
                e.prevent_default();

                if let Some(field) = e
                    .target()
                    .and_then(|t| t.dyn_into::<HtmlFormElement>().ok())
                    .and_then(|f| f.get_with_name("username"))
                    .and_then(|e| e.dyn_into::<HtmlInputElement>().ok())
                {
                    self.ws
                        .try_send(Request::Login {
                            name: field.value(),
                        })
                        .unwrap_throw();
                }
                false
            }
            AppBaseMsg::LobbyCodeSubmit(e) => {
                e.prevent_default();

                if let Some(field) = e
                    .target()
                    .and_then(|t| t.dyn_into::<HtmlFormElement>().ok())
                    .and_then(|f| f.get_with_name("code"))
                    .and_then(|e| e.dyn_into::<HtmlInputElement>().ok())
                {
                    self.ws
                        .try_send(Request::LobbyEnter {
                            name: field.value(),
                        })
                        .unwrap_throw();
                }
                false
            }
            AppBaseMsg::LobbyStart => {
                if self.lobby_data.is_some() {
                    self.ws.try_send(Request::LobbyStart).unwrap_throw();
                }
                false
            }
            AppBaseMsg::GameMove(mov) => {
                self.ws.try_send(Request::GameMove { mov }).unwrap_throw();
                false
            }
        }
    }

    fn changed(&mut self, _: &Context<Self>, _: &Self::Properties) -> bool {
        false
    }

    fn rendered(&mut self, ctx: &Context<Self>, first_render: bool) {
        if first_render {
            let local_storage = window().local_storage().unwrap_throw().unwrap_throw();
            let token = local_storage
                .get(&session_token_name(&ctx.props().name))
                .unwrap_throw();
            self.ws
                .try_send(Request::OpenSession { token })
                .unwrap_throw();
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let reload = ctx.link().callback(|_| AppBaseMsg::Reload);

        html! {
            <>
                <main class="app">
                    { match self.route {
                        Route::Splash => html! { <h1>{ctx.props().name.clone()}</h1> },
                        Route::Login => self.html_login(ctx),
                        Route::Lobby => self.html_lobby(ctx),
                        Route::Game => html! {
                            <>
                                { ctx.props().children.clone() }
                            </>
                        },
                    }}
                </main>
                <dialog open={!self.ws_connected && !matches!(self.route, Route::Splash)}>
                    <article>
                        <h3>{ "Disconnected" }</h3>
                        <p>{ "Unexpected disconnection, please reload the page." }</p>
                        <footer>
                            <button onclick={reload}>
                                { "Reload" }
                            </button>
                        </footer>
                    </article>
                </dialog>
            </>
        }
    }
}

impl<G: Game + 'static> AppBase<G> {
    fn html_login(&self, ctx: &Context<Self>) -> Html {
        let login_submit = ctx.link().callback(AppBaseMsg::LoginSubmit);
        html! {
            <article class="login">
                <header>
                    <h1>{ "Login" }</h1>
                </header>

                <form id="login" onsubmit={login_submit}>
                    <input
                        type="text"
                        name="username"
                        placeholder="Enter username"
                        value=""
                        autofocus=true
                    />
                    <button type="submit">{ "Login" }</button>
                </form>
            </article>
        }
    }

    fn html_lobby(&self, ctx: &Context<Self>) -> Html {
        let lobby_code_submit = ctx.link().callback(AppBaseMsg::LobbyCodeSubmit);
        let lobby_start = ctx.link().callback(|_| AppBaseMsg::LobbyStart);
        html! {
            <article class="lobby">
                <header>
                    <h1>{ "Lobby" }</h1>
                </header>

                <form id="lobby" onsubmit={lobby_code_submit}>
                    <input
                        type="text"
                        name="code"
                        placeholder="Enter lobby code"
                        value=""
                        autofocus=true
                    />
                    <button type="submit" >{ "Join lobby" }</button>
                </form>

                { if let Some(lobby_data) = &self.lobby_data {
                    html! {
                        <>
                            <h3>{ lobby_data.0.clone() }</h3>

                            <ul>
                                { lobby_data.1.iter().map(|p| html! {
                                    <li>{ p }</li>
                                }).collect::<Html>() }
                            </ul>

                            <footer>
                                <button
                                    class={classes!("btn", "btn-success", "btn-block")}
                                    onclick={lobby_start}
                                >
                                    { "Start game" }
                                </button>
                            </footer>
                        </>
                    }
                } else {
                    html! { <></> }
                }}
            </article>
        }
    }
}

fn session_token_name(name: &str) -> String {
    format!("{}_SESSION_TOKEN", name.to_uppercase())
}

impl<G: Game> Default for Parent<G> {
    fn default() -> Self {
        Self {
            username: Default::default(),
            mov: Callback::noop(),
            sound: Callback::noop(),
        }
    }
}
